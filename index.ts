import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import routes from './src/routes';
import globals from './src/globals';

const app = express();

app.use(bodyParser.json())
.use(cors())
.use(routes)
.listen(globals.PORT, () => {
    console.log(`listen on port ${globals.PORT}`);
})

