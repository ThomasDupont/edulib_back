import { Octokit } from '@octokit/rest';
import {
  GetResponseDataTypeFromEndpointMethod,
  Endpoints,
  OctokitResponse,
} from '@octokit/types';

const octokit = new Octokit();

export type ListOrgReposResponseDataType = OctokitResponse<GetResponseDataTypeFromEndpointMethod<
    typeof octokit.repos.listForOrg
>>;

export type GetRepoResponseDataType = OctokitResponse<GetResponseDataTypeFromEndpointMethod<
    typeof octokit.repos.get
>>;

export type ListOrgReposParameters = Endpoints['GET /orgs/{org}/repos']['parameters'];
export type GetRepoParameters = Endpoints['GET /repos/{owner}/{repo}']['parameters'];
