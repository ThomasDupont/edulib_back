import { Request } from 'express';

import * as githubClient from '../clients/github.client';
import { GetRepoResponseDataType, ListOrgReposResponseDataType } from '../types/github.types';
import { getRepository, listOrgRepositories } from './github';

const listOrgReposSpy = jest.spyOn(githubClient, 'listOrgRepos');
const getOrgRepoSpy = jest.spyOn(githubClient, 'getOrgRepo');

describe('Unit test for github', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });
  describe('listOrgRepositories method', () => {
    it('Should call listOrgRepos with a correct searchs params', async () => {
      listOrgReposSpy.mockResolvedValue({
        data: [],
      } as ListOrgReposResponseDataType);
      const req = {
        params: {
          org: 'octokit',
        },
        query: {
          page: '1',
        },
      } as unknown as Request;

      await listOrgRepositories(req);

      expect(listOrgReposSpy).toHaveBeenCalledWith({
        org: 'octokit',
        page: 1,
      });
    });
  });

  describe('getOrgRepo method method', () => {
    it('Should call getOrgRepo with a valid owner and a valid name', async () => {
      getOrgRepoSpy.mockResolvedValue({
        data: {
          name: 'octokit.rb',
        },
      } as GetRepoResponseDataType);
      const req = {
        params: {
          owner: 'octokit',
          repo: 'octokit.rb',
        },
      } as unknown as Request;
      await getRepository(req);

      expect(getOrgRepoSpy).toHaveBeenCalledWith({
        owner: 'octokit',
        repo: 'octokit.rb',
      });
    });
  });
});
