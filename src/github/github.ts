import { Request } from 'express';

import { listOrgRepos, getOrgRepo } from '../clients/github.client';

export async function listOrgRepositories(req: Request) {
  // A redis cache could be use here

  // For security, it is better to specify each params
  const data = await listOrgRepos({
    org: req.params.org,
    page: parseInt((req.query.page ?? '1').toString(), 10),
  });

  return data.data;
}

export async function getRepository(req: Request) {
  const repo = await getOrgRepo({
    owner: req.params.owner,
    repo: req.params.repo,
  });

  return repo.data;
}
