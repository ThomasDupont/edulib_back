import { Request } from 'express';

import { getRepository, listOrgRepositories } from './github';

describe('End To End test on github methods', () => {
  describe('listOrgRepositories method', () => {
    it('Should return a list of repo on known org', async () => {
      const req = {
        params: {
          org: 'octokit',
        },
        query: {
          page: '1',
        },
      } as unknown as Request;
      const repos = await listOrgRepositories(req);

      expect(repos.length).toBeGreaterThan(0);
      expect(repos[0].name).toBeDefined();
    });
  });

  describe('getRepository method', () => {
    it('Should return a single repo with a correct owner and a correct name', async () => {
      const req = {
        params: {
          owner: 'octokit',
          repo: 'octokit.rb',
        },
      } as unknown as Request;

      const repo = await getRepository(req);

      expect(repo).toBeDefined();
      expect(repo.name).toEqual('octokit.rb');
    });
  });
});
