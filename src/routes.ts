import {
  Response,
  Request,
  Router,
  NextFunction,
} from 'express';

import { listOrgRepositories, getRepository } from './github/github';

const router = Router();

// eslint-disable-next-line @typescript-eslint/ban-types
function handleResult<F extends Function>(method: F) {
  return async (req: Request, res: Response) => {
    try {
      const result = await method(req);
      return res.json(result);
    } catch (e) {
      // add an error monitoring here (For ex: Sentry)
      console.error(e);
      // On MS, the error status is 500, and the error handler is managed by the gateway
      return res.status(500).send({ err: e.message });
    }
  };
}

const middlewares = [
  function rateLimit(req: Request, res: Response, next: NextFunction) { return next(); },
  // whatever
];

type ListOrgRepos = typeof listOrgRepositories;
router.get('/github/orgs/:org/repos', middlewares, handleResult<ListOrgRepos>(listOrgRepositories));

type GetRepository = typeof getRepository;
router.get('/github/repos/:owner/:repo', middlewares, handleResult<GetRepository>(getRepository));

export default router;
