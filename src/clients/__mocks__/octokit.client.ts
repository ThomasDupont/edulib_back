export default {
  repos: {
    listForOrg: jest.fn(),
    get: jest.fn(),
  },
};
