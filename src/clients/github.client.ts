import {
  ListOrgReposParameters,
  ListOrgReposResponseDataType,
  GetRepoParameters,
  GetRepoResponseDataType,
} from '../types/github.types';
import octokit from './octokit.client';

export function listOrgRepos(params: ListOrgReposParameters): Promise<ListOrgReposResponseDataType> {
  return octokit.repos.listForOrg(params);
}

export function getOrgRepo(params: GetRepoParameters): Promise<GetRepoResponseDataType> {
  return octokit.repos.get(params);
}
