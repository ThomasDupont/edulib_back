import { Octokit } from '@octokit/rest';

import global from '../globals';

export default new Octokit({ auth: global.GITHUB_OAUTH_TOKEN });
