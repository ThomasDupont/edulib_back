import { ListOrgReposParameters, GetRepoParameters } from '../types/github.types';
import * as githubClient from './github.client';
import octokit from './octokit.client';

jest.mock('./octokit.client');

describe('Unit test for github client', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });
  describe('listOrgRepos method', () => {
    it('Should call octokit.repos.listForOrg with a correct searchs params', async () => {
      const args: ListOrgReposParameters = {
        org: 'org',
        page: 1,
      };

      await githubClient.listOrgRepos(args);

      expect(octokit.repos.listForOrg).toHaveBeenCalledWith(args);
    });
  });

  describe('getOrgRepo method', () => {
    it('Should call octokit.repos.get with a correct owner and repo name', async () => {
      const args: GetRepoParameters = {
        owner: 'org',
        repo: 'test',
      };

      await githubClient.getOrgRepo(args);

      expect(octokit.repos.get).toHaveBeenCalledWith(args);
    });
  });
});
