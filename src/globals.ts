export interface ProcessEnv {
  NODE_ENV: string;
  GITHUB_OAUTH_TOKEN: string;
  PORT: string;
}

// values from process.env can be undefined
type MaybeProcessEnv = Record<keyof ProcessEnv, string | undefined>;

const processEnv: ProcessEnv = getProcessEnvValues({
  NODE_ENV: process.env.NODE_ENV,
  GITHUB_OAUTH_TOKEN: process.env.GITHUB_OAUTH_TOKEN,
  PORT: process.env.PORT ?? '3000',
});

export default processEnv;

/**
   *
   * Private functions
   *
   */

function getProcessEnvValues(maybeProcessEnv: MaybeProcessEnv): ProcessEnv {
  if (isProcessEnv(maybeProcessEnv)) {
    return maybeProcessEnv;
  }

  const undefinedKeys = getUndefinedKeys(maybeProcessEnv);
  throw new Error(`
      Those process.env keys are missing: ${undefinedKeys.join(', ')}
      In development you should set those values in the (non-committed) file ".env"
    `);
}

function isProcessEnv(maybeProcessEnv: MaybeProcessEnv): maybeProcessEnv is ProcessEnv {
  return getUndefinedKeys(maybeProcessEnv).length === 0;
}

function getUndefinedKeys(processEnvValues: MaybeProcessEnv): Array<keyof ProcessEnv> {
  const envKeys = Object.keys(processEnvValues) as Array<keyof ProcessEnv>;
  return envKeys
    .reduce(
      (result, key) => (processEnvValues[key] === undefined
        ? [...result, key]
        : result),
      [] as Array<keyof ProcessEnv>,
    );
}
