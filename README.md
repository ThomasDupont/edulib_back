# Edulib test back

## Intallation & launch

Make sur to have Node:14 and Yarn installed

```sh
cp .env.dist .env
```

Get a github access token on [https://github.com/settings/tokens]

Add your GITHUB_OAUTH_TOKEN and your PORT on .env

And:

```sh
git clone https://gitlab.com/ThomasDupont/edulib_back.git edulib_back
cd edulib_back
yarn install
yarn start:dev
```

## Test

### Unit test
```sh
yarn test
```

### End to end test
```sh
yarn test:e2e
```

## Pre-production url

[https://edulib-back.herokuapp.com]

## Routes

### GET /github/orgs/:org/repos?page=

org: name of the organisation

page: desired result page number

### GET /github/repos/:owner/:repo

owner: name of the owner of the repo

repo: name of the repository

