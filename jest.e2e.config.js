module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  roots: ['<rootDir>/src/'],
  testMatch: [
    '<rootDir>/src/**/*.test-e2e.ts',
  ]
};
